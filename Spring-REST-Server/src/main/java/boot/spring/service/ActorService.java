package boot.spring.service;

import java.util.List;

import boot.spring.po.Actor;
import boot.spring.po.File;


public interface ActorService {
		List<Actor> getActors();
		Actor getActorByid(int id);
		Actor UpdateActor(Actor a);
		Actor SaveActor(Actor a);
		void Delete(int id);
		public File insertfile(File f);
}
